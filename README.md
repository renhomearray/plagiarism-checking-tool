# 查重工具 <img src="image/logo.svg" style="width:1em;height:1em;">

## 简单介绍

背景: 这是一个为了解 java swing 开发桌面端应用程序,而诞生的项目.

使用方案

1. 运行 App.java 中提供的 main 方法(或自行打包)
2. 选中你需要处理的文件所在目录
3. 筛选你需要的文件,并点击开始处理
4. 查看进度与处理日志,当处理完成后会出现下一步按钮
5. 此时你可以查看每一篇文章与其他文章的最高重复率,并查看每一篇文章与其他文章分别的重复率

## 使用截屏

![img.png](image/img.png)

![img_3.png](image/img_3.png)

![img_4.png](image/img_4.png)

![img_5.png](image/img_5.png)

![img_6.png](image/img_6.png)