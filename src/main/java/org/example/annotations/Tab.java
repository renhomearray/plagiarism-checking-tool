package org.example.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Tab {
    /**
     * 图标地址
     *
     * @return
     */
    String icon() default "";

    /**
     * 标签下标
     *
     * @return
     */
    int index() default -1;

    /**
     * 标题
     *
     * @return
     */
    String title() default "";

    /**
     * 提示信息
     *
     * @return
     */
    String tip() default "";

    Class clz() default Object.class;
}
