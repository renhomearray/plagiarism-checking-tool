package org.example.util;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class TempFileDetector {

    public static boolean isNotTemporaryFile(File file) {
        return !isTemporaryFile(file);
    }

    public static boolean isTemporaryFile(File file) {
        // 检查文件名
        if (file.getName().startsWith("~$")) {
            return true;
        }

        // 检查文件路径
        String tempDir = System.getProperty("java.io.tmpdir");
        return file.getParent() != null && file.getParent().equals(tempDir);
    }

    public static String readFile(String path) {
        path = path.replaceAll("^/(.*)","$1");
        ClassLoader classLoader = TempFileDetector.class.getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(path);

        if (inputStream != null) {
            try (Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8).useDelimiter("\\A")) {
                return scanner.hasNext() ? scanner.next() : "";
            }
        } else {
            System.err.println("File not found: " + path);
        }

        return null;
    }
}

