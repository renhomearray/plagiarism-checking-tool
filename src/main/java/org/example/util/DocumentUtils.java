package org.example.util;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.*;

/**
 * 获取一个文件的文档文本内容
 */
public class DocumentUtils {

    /**
     * 读取一个 Docx 文件的文本文档内容
     *
     * @param docxFile 文件对象
     * @return 读取的内容
     * @throws IOException 文件读取存在问题
     */
    public static String getDocxDocument(File docxFile) throws IOException {
        try (FileInputStream FileInputStream = new FileInputStream(docxFile)) {
            XWPFDocument xwpfDocument = new XWPFDocument(FileInputStream);
            XWPFWordExtractor xwpfWordExtractor = new XWPFWordExtractor(xwpfDocument);
            return xwpfWordExtractor.getText();
        }
    }

    /**
     * 读取一个 Doc 文件的文本文档内容
     *
     * @param docFile 文件对象
     * @return 读取的内容
     * @throws IOException 文件读取存在问题
     */
    public static String getDocDocument(File docFile) throws IOException {
        try (FileInputStream FileInputStream = new FileInputStream(docFile)) {
            HWPFDocument docDocument = new HWPFDocument(FileInputStream);
            WordExtractor docExtractor = new WordExtractor(docDocument);
            return docExtractor.getText();
        }
    }

    /**
     * 读取一个 Pdf 文件的文本文档内容
     *
     * @param pdfFile 文件对象
     * @return 读取的内容
     * @throws IOException 文件读取存在问题
     */
    public static String getPdfDocument(File pdfFile) throws IOException {
        try (PDDocument document = PDDocument.load(pdfFile)) {
            PDFTextStripper pdfStripper = new PDFTextStripper();
            return pdfStripper.getText(document);
        }
    }

    public static String getTextDocument(File file) {
        StringBuilder contentBuilder = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                contentBuilder.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return contentBuilder.toString();
    }
}
