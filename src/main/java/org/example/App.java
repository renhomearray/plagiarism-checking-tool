package org.example;

import com.formdev.flatlaf.FlatLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.formdev.flatlaf.fonts.jetbrains_mono.FlatJetBrainsMonoFont;
import com.formdev.flatlaf.themes.FlatMacDarkLaf;
import org.example.ui.MainFrame;
import org.example.ui.form.MainWindow;
import org.example.ui.jPanel.FileSelectTableJPanel;

import javax.swing.*;

public class App {
    public static MainFrame mainFrame;
    public static void main(String[] args) {
        FlatLaf.registerCustomDefaultsSource("themes");
        SwingUtilities.invokeLater(FlatJetBrainsMonoFont::install);

        //FlatMacDarkLaf.setup();
        FlatLightLaf.setup();
        // 定义主页面
        mainFrame = new MainFrame();
        mainFrame.init();
        // 查询文件
        FileSelectTableJPanel.getInstance().open();
        mainFrame.add(MainWindow.getInstance());
        mainFrame.setVisible(true);
        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        mainFrame.revalidate(); // 强制重新布局
        mainFrame.repaint(); // 强制重绘
    }
}