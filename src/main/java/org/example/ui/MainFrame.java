package org.example.ui;


import org.example.ui.jPanel.FileSelectTableJPanel;
import org.example.ui.listener.FrameListener;
import org.example.util.ComponentUtil;
import org.example.util.FrameUtil;

import javax.swing.*;

/**
 * <pre>
 * 主窗口
 * </pre>
 *
 * @author <a href="https://github.com/rememberber">Zhou Bo</a>
 * @since 2019/8/10.
 */
public class MainFrame extends JFrame {

    public void init() {
        // 设置名称顶端-头部等信息
        this.setName(UiConsts.APP_NAME);
        this.setTitle(UiConsts.APP_NAME);
        FrameUtil.setFrameIcon(this);

        //topMenuBar = TopMenuBar.getInstance();
        //topMenuBar.init();
        //setJMenuBar(topMenuBar);
        ComponentUtil.setPreferSizeAndLocateToCenter(this, 0.8, 0.88);
        FrameListener.addListeners(); // 添加监听程序
    }

}

