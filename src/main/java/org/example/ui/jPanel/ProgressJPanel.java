package org.example.ui.jPanel;

import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.formdev.flatlaf.ui.FlatUIUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.RepetitionDto;
import org.example.service.FileService;
import org.example.ui.form.MainWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 处理进度页
 */
@Getter
@Slf4j
public class ProgressJPanel extends JPanel {
    public static final String name = "ProgressJPanel";
    private static ProgressJPanel that;
    /**
     * 分析结果
     */
    private List<RepetitionDto> repetition;
    private LinkedHashMap<File, String> fileDocumentMap;
    private final JProgressBar progressBar;
    private final JLabel statusLabel;
    private final JTextArea logText;
    private final JPanel headerTitle;


    public ProgressJPanel() {

        setLayout(new BorderLayout());

        JPanel header = new JPanel();
        header.setLayout(new BorderLayout());

        headerTitle = new JPanel();
        headerTitle.setLayout(new BorderLayout());

        JLabel title = new JLabel("文件处理中");
        title.setFont(FlatUIUtils.nonUIResource(UIManager.getFont("h1.font"))); // 设置字体大小为h1
        headerTitle.add(title, BorderLayout.WEST);
        headerTitle.setBorder(new EmptyBorder(0, 0, 10, 0));
        header.add(headerTitle, BorderLayout.NORTH);

        progressBar = new JProgressBar();
        progressBar.setStringPainted(true); // 显示进度文本
        progressBar.setBorder(new EmptyBorder(0, 0, 10, 0));
        header.add(progressBar, BorderLayout.CENTER);

        statusLabel = new JLabel();
        statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
        statusLabel.setBorder(new EmptyBorder(0, 0, 10, 0));
        header.add(statusLabel, BorderLayout.SOUTH);

        add(header, BorderLayout.NORTH);


        logText = new JTextArea(5, 10);
        add(new JScrollPane(logText), BorderLayout.CENTER);
    }

    // 设置进度
    public void setProgress(int progress) {
        progressBar.setValue(progress);
    }

    // 设置当前正在进行的事情
    public void setStatus(String status) {
        statusLabel.setText(status);
        that.addLog("== [" + status + "] ===========================================");
    }

    public void addLog(String logStr) {
        logText.setText(logText.getText() + "\n" + logStr);
        logText.setCaretPosition(logText.getText().length());
        log.info(logStr);
    }

    public static ProgressJPanel getInstance() {
        if (that == null) {
            that = new ProgressJPanel();
        }
        return that;
    }

    public static void start() {
        getInstance(); // 保证已被初始化
        that.progressBar.setValue(0);
        that.setStatus("载入文件中……");
        File[] files = FileSelectTableJPanel.getInstance().getSelectFileByTable();
        that.setStatus("解析文件中……");
        that.fileDocumentMap = FileService.getInstance().analysisDocumentText(files, (file, integer) -> {
            that.setProgress(integer / 2); // 设置进度
            that.addLog(String.format("解析文件：%s", file.getAbsolutePath()));
        });
        that.setStatus("分析文件重复率中……");
        int value = that.progressBar.getValue();
        that.repetition = FileService.getInstance().repetition(that.fileDocumentMap, (file, integer) -> {
            that.setProgress(value + (integer / 2)); // 设置进度
            that.addLog(String.format("分析重复率：%s", file.getAbsolutePath()));
        });
        shouOkStart();

    }

    private static void shouOkStart() {
        JButton openButton = new JButton("查看对比结果");
        openButton.setIcon(new FlatSVGIcon("image/correct.svg", 20, 20));
        openButton.addActionListener(e -> {
            MainWindow.getInstance().goTo(ResultsJPanel.name);
            new Thread(ResultsJPanel::start).start();
        });
        that.headerTitle.add(openButton, BorderLayout.EAST);
        that.headerTitle.revalidate(); // 强制重新布局
        that.headerTitle.repaint(); // 强制重绘
    }
}
