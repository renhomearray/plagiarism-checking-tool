package org.example.ui.jPanel;

import com.formdev.flatlaf.ui.FlatUIUtils;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.RepetitionDto;
import org.example.ui.button.ButtonEditor;
import org.example.ui.button.ButtonRenderer;
import org.example.ui.form.MainWindow;
import org.example.ui.jDialog.ResultsJDialog;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Slf4j
public class ResultsJPanel extends JPanel {
    public static final String name = "ResultsJPanel";
    private static ResultsJPanel that;
    private JScrollPane scrollPane;

    public static void start() {
        log.info("查看文件分析结果");
        if (that.scrollPane != null) {
            that.remove(that.scrollPane);
        }
        List<RepetitionDto> repetition = ProgressJPanel.getInstance().getRepetition();
        that.scrollPane = new JScrollPane(initJTable(repetition));
        // 将滚动面板添加到 JPanel 的中心位置
        that.add(that.scrollPane, BorderLayout.CENTER);
        that.revalidate(); // 强制重新布局
        that.repaint(); // 强制重绘
    }

    private ResultsJPanel() {
        setLayout(new BorderLayout());
        add(getHeader(), BorderLayout.NORTH);
    }


    private static JTable initJTable(List<RepetitionDto> repetition) {
        // 创建一个表格模型
        DefaultTableModel model = new DefaultTableModel(0, 6);
        // 设置表格的列名
        model.setColumnIdentifiers(new Object[]{"序号", "最高重复率", "本文件", "最高重复文件", "查看明细", "查看文件"});
        JTable table = new JTable(model);

        table.setShowHorizontalLines(true);
        table.setShowVerticalLines(true);
        table.setIntercellSpacing(new Dimension(1, 1));
        table.setRowSelectionAllowed(true);
        //table.setRowHeight(30);

        // 设置最小宽度和固定宽度
        table.getColumnModel().getColumn(0).setMinWidth(40);
        table.getColumnModel().getColumn(0).setMaxWidth(40);
        table.getColumnModel().getColumn(1).setMinWidth(100);
        table.getColumnModel().getColumn(1).setMaxWidth(100);
        table.getColumnModel().getColumn(4).setMinWidth(100);
        table.getColumnModel().getColumn(4).setMaxWidth(100);
        table.getColumnModel().getColumn(4).setCellRenderer(new ButtonRenderer());
        table.getColumnModel().getColumn(4).setCellEditor(new ButtonEditor((row) -> {
            RepetitionDto repetitionDto = repetition.get(row);
            ResultsJDialog.getInstance().open(repetitionDto);
            log.info("打开查看明细弹窗 {}", repetitionDto.getFile().getAbsolutePath());
        }));
        table.getColumnModel().getColumn(5).setMinWidth(100);
        table.getColumnModel().getColumn(5).setMaxWidth(100);
        table.getColumnModel().getColumn(5).setCellRenderer(new ButtonRenderer());
        table.getColumnModel().getColumn(5).setCellEditor(new ButtonEditor((row) -> {
            File file = repetition.get(row).getFile();
            log.info("使用本地默认方式打开文件 {}", file.getAbsolutePath());
            try {
                Desktop.getDesktop().open(file);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }));
        DefaultTableCellRenderer defaultTableCellRenderer = new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (c instanceof JLabel label) {
                    label.setHorizontalAlignment(SwingConstants.LEFT);
                }
                return c;
            }
        };

        // 设置文件路径列的渲染器，使其左对齐并占据剩下的所有宽度
        table.getColumnModel().getColumn(2).setCellRenderer(defaultTableCellRenderer);
        table.getColumnModel().getColumn(3).setCellRenderer(defaultTableCellRenderer);

        for (int i = 0; i < repetition.size(); i++) {
            RepetitionDto repetitionDto = repetition.get(i);
            Map.Entry<File, Double> maxRepetition = repetitionDto.getMaxRepetition();
            model.addRow(new Object[]{i + 1,
                    String.format("%.2f", maxRepetition.getValue()),
                    repetitionDto.getFile().getName(),
                    maxRepetition.getValue().equals(0.0) ? "" : maxRepetition.getKey().getName(),
                    "查看明细", "查看文件"
            });
        }
        return table;
    }

    private static JPanel getHeader() {
        JPanel header = new JPanel();
        header.setLayout(new BorderLayout());

        JLabel title = new JLabel("文件处理结果");
        title.setFont(FlatUIUtils.nonUIResource(UIManager.getFont("h1.font"))); // 设置字体大小为h1
        header.add(title, BorderLayout.WEST);

        // 添加按钮组
        header.add(getButtonPan(), BorderLayout.EAST);

        header.setBorder(new EmptyBorder(0, 0, 10, 0));
        return header;
    }

    /**
     * 构建一个按钮组
     *
     * @return 按钮组
     */
    private static JPanel getButtonPan() {
        // 构建按钮组
        JPanel buttonPan = new JPanel();
        buttonPan.setLayout(new BorderLayout());

        JButton openButton = new JButton("重新选择文件");
        openButton.addActionListener(e -> {
            MainWindow.getInstance().goTo(FileSelectTableJPanel.name);
            new Thread(() -> FileSelectTableJPanel.getInstance().open()).start();
        });
        buttonPan.add(openButton, BorderLayout.WEST);
        return buttonPan;
    }

    public static ResultsJPanel getInstance() {
        if (that == null) {
            that = new ResultsJPanel();
        }
        return that;
    }


}
