package org.example.ui.jPanel;

import cn.hutool.core.util.BooleanUtil;
import com.formdev.flatlaf.extras.FlatSVGIcon;
import com.formdev.flatlaf.ui.FlatUIUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.example.ui.form.MainWindow;
import org.example.ui.jDialog.HtmlJDialog;
import org.example.util.TempFileDetector;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Slf4j
@Getter
public class FileSelectTableJPanel extends JPanel {
    public static final String name = "FileSelectTableJPanel";
    private static FileSelectTableJPanel that;
    private JFileChooser fileChooser;
    private JScrollPane scrollPane;
    private static final JPanel okHeader = initOkHeader();
    private static final JPanel errorHeader = initErrorHeader();
    private JTable table;

    private FileSelectTableJPanel() {
        this.setLayout(new BorderLayout());
        new Thread(this::initFileChooser).start();
    }

    private void initFileChooser() {
        this.add(errorHeader, BorderLayout.NORTH);
        this.fileChooser = new JFileChooser();
        this.fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }

    public void open() {
        if(table != null){
            this.remove(okHeader);
            this.remove(scrollPane);
            this.revalidate(); // 强制重新布局
            this.repaint(); // 强制重绘
            this.add(errorHeader, BorderLayout.NORTH);
        }
        int i1 = JOptionPane.showConfirmDialog(
                this,
                "使用功能前，请先选择你要处理的文档所在文件夹。",
                "提示",
                JOptionPane.YES_NO_OPTION
        );
        if (i1 == 0) {
            if (fileChooser == null) {
                initFileChooser();
            }
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                if (file.isDirectory()) {
                    this.remove(errorHeader);
                    this.add(okHeader, BorderLayout.NORTH);
                    File[] files = file.listFiles();
                    if (files != null && files.length > 0) {
                        files = Stream.of(files).filter(File::isFile).filter(TempFileDetector::isNotTemporaryFile).toArray(File[]::new);
                        table = initJTable(files);
                        // 创建一个滚动面板，并将表格添加到其中
                        scrollPane = new JScrollPane(table);
                        // 将滚动面板添加到 JPanel 的中心位置
                        this.add(scrollPane, BorderLayout.CENTER);
                        this.revalidate(); // 强制重新布局
                        this.repaint(); // 强制重绘
                    }
                    return;
                } else {
                    table = null;
                }
            }
        }
        this.add(errorHeader, BorderLayout.NORTH);
    }

    public static FileSelectTableJPanel getInstance() {
        if (that == null) {
            that = new FileSelectTableJPanel();
        }
        return that;
    }

    private JTable initJTable(File[] files) {
        // 创建一个表格模型
        DefaultTableModel model = new DefaultTableModel(0, 3) {
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 1) { // 设置选择列的数据类型为Boolean
                    return Boolean.class;
                } else if (columnIndex == 2) {
                    return File.class;
                } else {
                    return String.class;
                }
            }

        };
        // 设置表格的列名
        model.setColumnIdentifiers(new Object[]{"序", "号", "文件路径"});
        JTable table = new JTable(model);

        // 设置第一列和第二列的最小宽度和固定宽度
        table.getColumnModel().getColumn(0).setMinWidth(20);
        table.getColumnModel().getColumn(0).setMaxWidth(20);
        table.getColumnModel().getColumn(1).setMinWidth(20);
        table.getColumnModel().getColumn(1).setMaxWidth(20);

        // 设置文件路径列的渲染器，使其左对齐并占据剩下的所有宽度
        table.getColumnModel().getColumn(2).setCellRenderer(new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (c instanceof JLabel label) {
                    label.setHorizontalAlignment(SwingConstants.LEFT);
                }
                return c;
            }
        });

        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            model.addRow(new Object[]{i + 1, switch (FilenameUtils.getExtension(file.getName())) {
                case "pdf", "docx", "doc" -> true;
                default -> false;
            }, file});
        }
        return table;
    }

    public File[] getSelectFileByTable() {
        if(table == null){
            return new File[0];
        }
        TableModel model = table.getModel();
        int rowCount = model.getRowCount();
        List<File> fileList = new ArrayList<>(rowCount);
        for (int i = 0; i < rowCount; i++) {
            Boolean select = (Boolean) model.getValueAt(i, 1);
            if (BooleanUtil.isTrue(select)) {
                File file = (File) model.getValueAt(i, 2);
                fileList.add(file);
            }
        }
        return fileList.toArray(File[]::new);
    }

    private static JPanel initOkHeader() {
        JPanel header = new JPanel();
        header.setLayout(new BorderLayout());
        JLabel title = new JLabel("你选中了以下文件");
        title.setFont(FlatUIUtils.nonUIResource(UIManager.getFont("h1.font"))); // 设置字体大小为h1
        header.add(title, BorderLayout.WEST);

        JButton openButton = new JButton("开始对比");
        openButton.setIcon(new FlatSVGIcon("image/lookingFor.svg", 20, 20));
        openButton.addActionListener(e -> {
            MainWindow.getInstance().goTo(ProgressJPanel.name);
            new Thread(ProgressJPanel::start).start();
        });
        header.add(openButton, BorderLayout.EAST);
        header.setBorder(new EmptyBorder(0, 0, 10, 0));

        return header;
    }
    private static JPanel initErrorHeader() {
        JPanel header = new JPanel();
        header.setLayout(new BorderLayout());
        JLabel title = new JLabel("你没有选择文件");
        title.setFont(FlatUIUtils.nonUIResource(UIManager.getFont("h1.font"))); // 设置字体大小为h1
        header.add(title, BorderLayout.WEST);

        //JButton openButton = new JButton("打开弹窗");
        //openButton.setIcon(new FlatSVGIcon("image/lookingFor.svg", 20, 20));
        //openButton.setBackground(new Color(0, 122, 255));
        //openButton.addActionListener(e -> {
        //    HtmlJDialog.getInstance().open3();
        //});
        //header.add(openButton, BorderLayout.EAST);
        //header.setBorder(new EmptyBorder(0, 0, 10, 0));

        return header;
    }
}
