package org.example.ui.button;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

public class ButtonRenderer implements TableCellRenderer {

    private final JButton button;

    public ButtonRenderer() {
        button = new JButton();
    }

    @Override
    public JButton getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        button.setText((value == null) ? "" : value.toString());
        return button;
    }
}