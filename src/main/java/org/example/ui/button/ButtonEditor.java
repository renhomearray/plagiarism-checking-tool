package org.example.ui.button;

import javax.swing.*;
import java.util.function.Consumer;

public class ButtonEditor extends DefaultCellEditor {

    private final JButton button;
    private int row;

    private String label;

    private boolean isPushed;

    private final Consumer<Integer> consumer;

    public ButtonEditor(Consumer<Integer> consumer) {
        super(new JCheckBox());
        this.button = new JButton();
        this.button.addActionListener(e -> fireEditingStopped());
        this.consumer = consumer;
    }

    @Override
    public JButton getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        this.label = (value == null) ? "" : value.toString();
        this.button.setText(label);
        this.isPushed = true;
        this.row = row;
        return button;
    }

    @Override
    public Object getCellEditorValue() {
        if (this.isPushed) {
            this.consumer.accept(row);
        }
        this.isPushed = false;
        return label;
    }

    @Override
    public boolean stopCellEditing() {
        isPushed = false;
        return super.stopCellEditing();
    }
}
