package org.example.ui.jDialog;

import lombok.extern.slf4j.Slf4j;
import org.example.App;
import org.example.dto.RepetitionDto;
import org.example.ui.button.ButtonEditor;
import org.example.ui.button.ButtonRenderer;
import org.example.util.ComponentUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Map;

@Slf4j
public class ResultsJDialog extends JDialog {
    private static ResultsJDialog that;
    private JScrollPane scrollPane;
    public ResultsJDialog() {
        super(App.mainFrame);
        setLayout(new BorderLayout());
        setVisible(false); // 不显示对话框
        //setSize(new Dimension(200,150));
        ComponentUtil.setPreferSizeAndLocateToCenter(this, 0.6, 0.5);
    }

    public void open(RepetitionDto repetitionDto) {
        String titleText = String.format("[%s] 重复率", repetitionDto.getFile().getName());
        this.setTitle(titleText);
        if(scrollPane != null){
            this.remove(scrollPane);
        }
        scrollPane = new JScrollPane(initJTable(repetitionDto));
        this.add(scrollPane, BorderLayout.CENTER);
        //this.revalidate(); // 强制重新布局
        //this.repaint(); // 强制重绘
        this.setVisible(true); // 显示对话框
    }

    private static JTable initJTable(RepetitionDto repetitionDto) {
        File[] repetitionFileArray = repetitionDto.getRepetitionFileArray();
        // 创建一个表格模型
        DefaultTableModel model = new DefaultTableModel(0, 4);
        // 设置表格的列名
        model.setColumnIdentifiers(new Object[]{"序号", "重复率", "重复文件", "查看文件", "查看对比结果"});
        JTable table = new JTable(model);

        table.setShowHorizontalLines(true);
        table.setShowVerticalLines(true);
        table.setIntercellSpacing(new Dimension(1, 1));
        table.setRowSelectionAllowed(true);
        //table.setRowHeight(30);

        // 设置最小宽度和固定宽度
        table.getColumnModel().getColumn(0).setMinWidth(40);
        table.getColumnModel().getColumn(0).setMaxWidth(40);
        table.getColumnModel().getColumn(1).setMinWidth(100);
        table.getColumnModel().getColumn(1).setMaxWidth(100);

        table.getColumnModel().getColumn(3).setMinWidth(100);
        table.getColumnModel().getColumn(3).setMaxWidth(100);
        table.getColumnModel().getColumn(3).setCellRenderer(new ButtonRenderer());
        table.getColumnModel().getColumn(3).setCellEditor(new ButtonEditor((row) -> {
            File file = repetitionFileArray[row];
            log.info("使用本地默认方式打开文件 {}", file.getAbsolutePath());
            try {
                Desktop.getDesktop().open(file);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }));
        table.getColumnModel().getColumn(4).setMinWidth(150);
        table.getColumnModel().getColumn(4).setMaxWidth(150);
        table.getColumnModel().getColumn(4).setCellRenderer(new ButtonRenderer());
        table.getColumnModel().getColumn(4).setCellEditor(new ButtonEditor((row) -> {
            File file = repetitionFileArray[row];
            log.info("查看对比结果 {}", file.getAbsolutePath());
            HtmlJDialog.getInstance().open(repetitionDto.getFile(),file);
        }));

        // 设置文件路径列的渲染器，使其左对齐并占据剩下的所有宽度
        table.getColumnModel().getColumn(2).setCellRenderer(new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (c instanceof JLabel label) {
                    label.setHorizontalAlignment(SwingConstants.LEFT);
                }
                return c;
            }
        });

        int index = 1;
        for (Map.Entry<File, Double> entry : repetitionDto.getRepetitionMap().entrySet()) {
            model.addRow(new Object[]{index++,
                    String.format("%.2f", entry.getValue()),
                    entry.getKey().getName(), "查看文件","查看对比结果"
            });
        }
        return table;
    }

    public static ResultsJDialog getInstance() {
        if (that == null) {
            that = new ResultsJDialog();
        }
        return that;
    }
}
