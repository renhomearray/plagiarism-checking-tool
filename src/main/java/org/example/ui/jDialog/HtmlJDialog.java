package org.example.ui.jDialog;

import cn.hutool.core.io.file.FileNameUtil;
import com.jetbrains.cef.JCefAppConfig;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefRendering;
import org.example.App;
import org.example.service.DiffHtmlService;
import org.example.ui.jPanel.FileSelectTableJPanel;
import org.example.util.ComponentUtil;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.net.URI;

public class HtmlJDialog extends JDialog {

    private URI diffFolder;

    private static HtmlJDialog that;

    public HtmlJDialog() {
        super(App.mainFrame);
        setLayout(new BorderLayout());
        setVisible(false); // 不显示对话框
        ComponentUtil.setPreferSizeAndLocateToCenter(this, 0.6, 0.5);

    }

    public URI getDiffFolder() {
        boolean isInit = false;
        if (diffFolder == null) {
            isInit = true;
        } else {
            File file = new File(diffFolder);
            if (!(file.exists() && file.isDirectory())) {
                isInit = true;
            }
        }
        if (isInit) {
            if (0 == JOptionPane.showConfirmDialog(
                    this,
                    "使用功能前，请先选择对比结果将来保存的目标文件夹。",
                    "提示",
                    JOptionPane.YES_NO_OPTION
            )) {
                JFileChooser fileChooser = FileSelectTableJPanel.getInstance().getFileChooser();
                int result = fileChooser.showOpenDialog(App.mainFrame);
                if (result == JFileChooser.APPROVE_OPTION) {
                    diffFolder = fileChooser.getSelectedFile().toURI();
                }
            }
        }
        return diffFolder;
    }

    private CefApp app = null;
    private CefClient client = null;
    private Component uiComponent;

    public static HtmlJDialog getInstance() {
        if (that == null) {
            that = new HtmlJDialog();
        }
        return that;
    }

    public void open(File originalFile, File revisedFile) {
        URI diffHtml = DiffHtmlService.getDiffHtml(originalFile, revisedFile);
        if (diffHtml == null) {
            return;
        }
        File file = new File(diffHtml);

        String titleText = FileNameUtil.getPrefix(file.getName());
        this.setTitle(titleText);

        if (app == null) {
            var args = JCefAppConfig.getInstance().getAppArgs();
            var settings = JCefAppConfig.getInstance().getCefSettings();
            settings.cache_path = System.getProperty("user.dir") + "/context";
            // 获取CefApp实例
            CefApp.startup(args);
            app = CefApp.getInstance(args, settings);
        }
        if (client == null) {
            // 创建客户端实例
            client = app.createClient();
        }
        if(uiComponent != null){
            this.remove(uiComponent);
        }
        uiComponent = client.createBrowser(file.getPath(), CefRendering.DEFAULT, true).getUIComponent();
        this.add(uiComponent, BorderLayout.CENTER);
        this.revalidate(); // 强制重新布局
        this.repaint(); // 强制重绘
        this.setVisible(true); // 显示对话框
    }
}
