package org.example.ui.form;

import lombok.Getter;
import org.example.ui.jPanel.FileSelectTableJPanel;
import org.example.ui.jPanel.ProgressJPanel;
import org.example.ui.jPanel.ResultsJPanel;

import javax.swing.*;
import java.awt.*;

@Getter
public class MainWindow extends JPanel {

    private final CardLayout layout;

    private static MainWindow mainWindow;

    public void goTo(String constraints) {
        layout.show(this, constraints);
    }

    private MainWindow() {
        layout = new CardLayout(10, 10);
        this.setLayout(layout);
        this.add(FileSelectTableJPanel.getInstance(), FileSelectTableJPanel.name);
        this.add(ProgressJPanel.getInstance(), ProgressJPanel.name);
        this.add(ResultsJPanel.getInstance(), ResultsJPanel.name);
    }

    public static MainWindow getInstance() {
        if (mainWindow == null) {
            mainWindow = new MainWindow();
        }
        return mainWindow;
    }

}
