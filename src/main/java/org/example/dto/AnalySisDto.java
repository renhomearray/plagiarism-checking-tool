package org.example.dto;

import lombok.Getter;

import java.io.File;

@Getter
public class AnalySisDto {
    private final File file;
    private final int index;

    public AnalySisDto(File file, int index) {
        this.file = file;
        this.index = index;
    }
}
