package org.example.dto;

import lombok.Getter;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 重复率信息
 */

@Getter
public class RepetitionDto {
    /**
     * 当前代表的文件
     */
    private final File file;

    Map<File, Double> repetitionMap;

    public File[] getRepetitionFileArray(){
        File[] files = new File[repetitionMap.size()];
        int index = 0;
        for (Map.Entry<File, Double> entry : repetitionMap.entrySet()) {
            files[index++] = entry.getKey();
        }
        return files;
    }

    /**
     * 获取最高重复率
     *
     * @return {重复的文件，重复几率}
     */
    public Map.Entry<File, Double> getMaxRepetition() {
        Set<Map.Entry<File, Double>> entries = repetitionMap.entrySet();
        Double value = -1.0;
        Map.Entry<File, Double> result = null;
        for (Map.Entry<File, Double> entry : entries) {
            if (entry.getValue() > value) {
                result = entry;
            }
        }
        return result;
    }

    /**
     * 设置重复率
     *
     * @param file       重复的文件
     * @param repetition 和该文件的重复率
     */
    public void addRepetition(File file, Double repetition) {
        repetitionMap.put(file, repetition);
    }

    public RepetitionDto(File file) {
        this.file = file;
        this.repetitionMap = new HashMap<>();
    }

}
