package org.example.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.example.dto.RepetitionDto;
import org.example.ui.jPanel.ProgressJPanel;
import org.example.util.DocumentUtils;

import java.io.File;
import java.util.*;
import java.util.function.BiConsumer;

@Slf4j
public class FileService {
    private static FileService fileService;

    public FileService() {
    }

    public static List<String> parsingLines(File originalFile) {
        LinkedHashMap<File, String> fileDocumentMap = ProgressJPanel.getInstance().getFileDocumentMap();
        String result = fileDocumentMap.get(originalFile);
        String[] split = result.split("\n");
        return Arrays.asList(split);
    }

    /**
     * 读取一系列文件中的文档主体内容，并实时更新进度
     *
     * @param files    文档内容
     * @param consumer 用于外界感知进度的接口
     * @return 读取的文档内容 LinkedHashMap
     */
    public LinkedHashMap<File, String> analysisDocumentText(File[] files, BiConsumer<File, Integer> consumer) {
        LinkedHashMap<File, String> result = new LinkedHashMap<>(files.length);
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            consumer.accept(file, (i + 1) * 100 / files.length);
            String extension = FilenameUtils.getExtension(file.getName());
            try {
                String document = switch (extension.toLowerCase()) {
                    case "doc" -> DocumentUtils.getDocDocument(file);
                    case "docx" -> DocumentUtils.getDocxDocument(file);
                    case "pdf" -> DocumentUtils.getPdfDocument(file);
                    default -> DocumentUtils.getTextDocument(file);
                };
                result.put(file, document);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return result;
    }

    /**
     * 按照文件内容对文件执行比对
     *
     * @param documentMap 希望比较的文档集合
     * @param consumer    用于外界感知进度的接口
     * @return 比较结果集合
     */
    public List<RepetitionDto> repetition(LinkedHashMap<File, String> documentMap, BiConsumer<File, Integer> consumer) {
        List<RepetitionDto> result = new ArrayList<>(documentMap.size());
        int totalFiles = documentMap.size();
        int fileCount = 0;

        for (Map.Entry<File, String> entry : documentMap.entrySet()) {
            consumer.accept(entry.getKey(), ++fileCount * 100 / totalFiles);
            String fileName = entry.getKey().getName();
            String content = entry.getValue();
            RepetitionDto item = new RepetitionDto(entry.getKey());

            // 计算每一篇文章的重复率
            documentMap.forEach((file, document) -> {
                if (!file.getName().equals(fileName)) {
                    double similarity = calculateSimilarity(content, document);
                    item.addRepetition(file, similarity);
                }
            });
            result.add(item);
        }
        return result;
    }

    private static double calculateSimilarity(String text1, String text2) {
        LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
        return 1 - (double) levenshteinDistance.apply(text1, text2) / Math.max(text1.length(), text2.length());
    }

    public static FileService getInstance() {
        if (fileService == null) {
            fileService = new FileService();
        }
        return fileService;
    }
}
