package org.example.service;

import cn.hutool.core.io.file.FileNameUtil;
import com.github.difflib.UnifiedDiffUtils;
import com.github.difflib.patch.Patch;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.RepetitionDto;
import org.example.ui.jDialog.HtmlJDialog;
import org.example.ui.jPanel.ProgressJPanel;
import org.example.util.TempFileDetector;

import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author 西凉的悲伤
 * <a href="https://blog.csdn.net/qq_33697094/article/details/121767084">参考文档</a>
 * <a href="https://diff2html.xyz/index.html#cli">工具网站</a>
 */
@Slf4j
public class DiffHtmlService {


    /**
     * 比较两篇文章，并获取比较结果路径
     *
     * @param originalFile 原创文章
     * @param revisedFile  修订结果
     * @return
     */
    public static URI getDiffHtml(File originalFile, File revisedFile) {
        URI diffFolder = HtmlJDialog.getInstance().getDiffFolder();
        if (diffFolder == null) {
            return null;
        }
        initResources();
        File resultDirectory = new File(String.join(File.separator, diffFolder.getPath(), FileNameUtil.getPrefix(originalFile)));
        //noinspection ResultOfMethodCallIgnored
        resultDirectory.mkdirs();

        /* 如果文件存在，则直接使用已存在的文件 */
        String fileName = String.format("%s→%s.html", originalFile.getName(), revisedFile.getName());
        File resultFile = new File(String.join(File.separator, diffFolder.getPath(), FileNameUtil.getPrefix(originalFile), fileName));
        if (resultFile.exists() && resultFile.isFile()) {
            return resultFile.toURI();
        }
        /* 文件不存在，则执行文件比对并声称比对结果 */
        List<String> original = FileService.parsingLines(originalFile);
        List<String> revised = FileService.parsingLines(revisedFile);
        List<String> diffList = diffString(original, revised, originalFile.getName(), revisedFile.getName());
        String result = generateDiffHtmlStr(diffList, fileName);
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(resultFile), StandardCharsets.UTF_8))) {
            writer.write(result);
            log.info("生成比对结果 {}", resultFile.getName());
        } catch (IOException e) {
            log.error("写入比对结果出错");
        }
        return resultFile.toURI();
    }


    /**
     * 生成所有文件的比对结果
     *
     * @return
     */
    public URI getDiffHtmlAll(Double minRepetition) {
        URI diffFolder = HtmlJDialog.getInstance().getDiffFolder();
        if (diffFolder == null) {
            return null;
        }
        initResources();
        List<RepetitionDto> repetitionList = ProgressJPanel.getInstance().getRepetition();
        for (RepetitionDto item : repetitionList) {
            File originalFile = item.getFile();
            item.getRepetitionMap().forEach((revisedFile, repetition) -> {
                if (repetition >= minRepetition) {
                    getDiffHtml(originalFile, revisedFile);
                }
            });
        }
        return HtmlJDialog.getInstance().getDiffFolder();
    }

    /**
     * 初始化 resources
     */
    private static void initResources() {
        URI diffFolder = HtmlJDialog.getInstance().getDiffFolder();
        File resourcesFile = new File(diffFolder.getPath() + File.separator + "/resources");
        if (resourcesFile.exists() && resourcesFile.isDirectory()) {
            return;
        }
        resourcesFile.mkdirs();
        String path = resourcesFile.getPath();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + File.separator + "diff.css"))) {
            writer.write(Objects.requireNonNull(TempFileDetector.readFile("/template/diff.css")));
        } catch (IOException e) {
            log.error("写入diff.css出错");
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + File.separator + "diff.js"))) {
            writer.write(Objects.requireNonNull(TempFileDetector.readFile("/template/diff.js")));
        } catch (IOException e) {
            log.error("写入diff.js出错");
        }
    }

    /**
     * 根据对比结果获取最终对比结果
     *
     * @param diffList diffString 方法生成的结果
     * @return
     */
    private static String generateDiffHtmlStr(List<String> diffList, String fileName) {
        String template = TempFileDetector.readFile("/template/diff.template.html");
        String diffString = String.join("\n", diffList);
        if (template != null) {
            return template.replace("titleStr", fileName)
                    .replace("@@@@@@@@@@@@@@@@@@@@@@@@@@@@", diffString);
        }
        return "";
    }

    /**
     * 对比两文件的差异，返回原始文件+diff格式
     *
     * @param original         原文件内容
     * @param revised          对比文件内容
     * @param originalFileName 原始文件名
     * @param revisedFileName  对比文件名
     */
    public static List<String> diffString(List<String> original, List<String> revised, String originalFileName, String revisedFileName) {
        originalFileName = originalFileName == null ? "原始文件" : originalFileName;
        revisedFileName = revisedFileName == null ? "对比文件" : revisedFileName;
        //两文件的不同点
        Patch<String> patch = com.github.difflib.DiffUtils.diff(original, revised);
        //生成统一的差异格式
        List<String> unifiedDiff = UnifiedDiffUtils.generateUnifiedDiff(originalFileName, revisedFileName, original, patch, 0);
        int diffCount = unifiedDiff.size();
        if (unifiedDiff.size() == 0) {
            //如果两文件没差异则插入如下
            unifiedDiff.add("--- " + originalFileName);
            unifiedDiff.add("+++ " + revisedFileName);
            unifiedDiff.add("@@ -0,0 +0,0 @@");
        } else if (unifiedDiff.size() >= 3 && !unifiedDiff.get(2).contains("@@ -1,")) {
            unifiedDiff.set(1, unifiedDiff.get(1));
            //如果第一行没变化则插入@@ -0,0 +0,0 @@
            unifiedDiff.add(2, "@@ -0,0 +0,0 @@");
        }
        //原始文件中每行前加空格
        List<String> original1 = original.stream().map(v -> " " + v).collect(Collectors.toList());
        //差异格式插入到原始文件中
        return insertOrig(original1, unifiedDiff);
    }


    //统一差异格式插入到原始文件
    public static List<String> insertOrig(List<String> original, List<String> unifiedDiff) {
        List<String> result = new ArrayList<>();
        //unifiedDiff中根据@@分割成不同行，然后加入到diffList中
        List<List<String>> diffList = new ArrayList<>();
        List<String> d = new ArrayList<>();
        for (int i = 0; i < unifiedDiff.size(); i++) {
            String u = unifiedDiff.get(i);
            if (u.startsWith("@@") && !"@@ -0,0 +0,0 @@".equals(u) && !u.contains("@@ -1,")) {
                List<String> twoList = new ArrayList<>();
                twoList.addAll(d);
                diffList.add(twoList);
                d.clear();
                d.add(u);
                continue;
            }
            if (i == unifiedDiff.size() - 1) {
                d.add(u);
                List<String> twoList = new ArrayList<>();
                twoList.addAll(d);
                diffList.add(twoList);
                d.clear();
                break;
            }
            d.add(u);
        }

        //将diffList和原始文件original插入到result，返回result
        for (int i = 0; i < diffList.size(); i++) {
            List<String> diff = diffList.get(i);
            List<String> nexDiff = i == diffList.size() - 1 ? null : diffList.get(i + 1);
            //含有@@的一行
            String simb = i == 0 ? diff.get(2) : diff.get(0);
            String nexSimb = nexDiff == null ? null : nexDiff.get(0);
            //插入到result
            insert(result, diff);
            //解析含有@@的行，得到原文件从第几行开始改变，改变了多少（即增加和减少的行）
            Map<String, Integer> map = getRowMap(simb);
            if (null != nexSimb) {
                Map<String, Integer> nexMap = getRowMap(nexSimb);
                int start = 0;
                if (map.get("orgRow") != 0) {
                    start = map.get("orgRow") + map.get("orgDel") - 1;
                }
                int end = nexMap.get("orgRow") - 2;
                //插入不变的
                insert(result, getOrigList(original, start, end));
            }

            int start = (map.get("orgRow") + map.get("orgDel") - 1);
            start = start == -1 ? 0 : start;
            if (simb.contains("@@ -1,") && null == nexSimb && map.get("orgDel") != original.size()) {
                insert(result, getOrigList(original, start, original.size() - 1));
            } else if (null == nexSimb && (map.get("orgRow") + map.get("orgDel") - 1) < original.size()) {
                insert(result, getOrigList(original, start, original.size() - 1));
            }
        }
        //如果你想知道两文件有几处不同可以放开下面5行代码注释，会在文件名后显示总的不同点有几处（即预览图中的xxx different），放开注释后有一个小缺点就是如果对比的是java、js等代码文件那代码里的关键字就不会高亮颜色显示,有一点不美观。
        //int diffCount = diffList.size() - 1;
        //if (!"@@ -0,0 +0,0 @@".equals(unifiedDiff.get(2))) {
        //    diffCount = diffList.size() > 1 ? diffList.size() : 1;
        //}
        //result.set(1, result.get(1) + " ( " + diffCount + " different )");
        return result;
    }

    //将源文件中没变的内容插入result
    private static void insert(List<String> result, List<String> noChangeContent) {
        for (String ins : noChangeContent) {
            result.add(ins);
        }
    }

    //解析含有@@的行得到修改的行号删除或新增了几行
    private static Map<String, Integer> getRowMap(String str) {
        Map<String, Integer> map = new HashMap<>();
        //正则校验字符串是不是 "@@ -3,1 +3,1 @@" 这样的diff格式
        String pattern = "^@@\\s-*\\d+,\\d+\\s+\\+\\d+,\\d+\\s+@@$";
        if (Pattern.matches(pattern, str)) {
            String[] sp = str.split(" ");
            String org = sp[1];
            String[] orgSp = org.split(",");
            //源文件要删除行的行号
            map.put("orgRow", Integer.valueOf(orgSp[0].substring(1)));
            //源文件删除的行数
            map.put("orgDel", Integer.valueOf(orgSp[1]));

            String[] revSp = sp[2].split(",");
            //对比文件要增加行的行号
            map.put("revRow", Integer.valueOf(revSp[0].substring(1)));
            map.put("revAdd", Integer.valueOf(revSp[1]));
        }
        return map;
    }

    //从原文件中获取指定的部分行
    private static List<String> getOrigList(List<String> original1, int start, int end) {
        List<String> list = new ArrayList<>();
        if (original1.size() >= 1 && start <= end && end < original1.size()) {
            for (; start <= end; start++) {
                list.add(original1.get(start));
            }
        }
        return list;
    }

}
